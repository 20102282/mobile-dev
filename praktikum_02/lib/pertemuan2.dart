import 'animal.dart';

void main() {
  var nilai = "A";
  if (nilai == 'A') {
    print('sangat baik');
  } else if (nilai == 'B') {
    print('cukup');
  } else {
    print('cukup');
  }

//conditiion ? statement1 : statement2
  (nilai == 'A')
      ? print('sangat baik')
      : (nilai == 'B')
          ? print('baik')
          : print('cukup');

// Switch Case
  switch (nilai) {
    case 'A':
      print('Sangat Bagus');
      break;
    case 'B':
      print('Bagus');
      break;
    case 'C':
      print('Cukup');
      break;
  }

  // perulangan
  // for loop
  var listku = [1, 2, 3, 4, 'Lima'];
  for (int i = 0; i < 5; i++) {
    print(listku[i]);
  }

  // for each
  listku.forEach((element) {
    print(element);
  });

  // for in
  for (var item in listku) {
    print(item);
  }

  // while
  var a = 1;
  while (a <= 100) {
    print(a);
    a++;
  }

  // Do while
  do {
    print(a);
    a++;
  } while (a <= 10);

// List
  var listSaya = [2, 'Saya', true];

  // Set
  Set angkaSet = {1, 2, 2, 3, 4, 4};

  // Map
  var kota = {
    'Semarang': 'Jawa tengah',
    'Bandung': 'Jawa Barat',
    'Malang': 'Jawa Timur',
  };

  //objek
  // Animal a = Animal
  Anjing doggy = Anjing('Doggy', 20.0);
  doggy.bersuara();
  doggy.lari();

  Burung b = Burung('b', 10.0, 2);
  b.bersuara();
  b.terbang();

  //ASinkronus
  Future<String> getProduct() {
    return Future.delayed(Duration(seconds: 3), () {
      var isProductAvailable = false;
      if (isProductAvailable) {
        return 'coffe boba';
      } else {
        throw 'our stock is not enough';
      }
      return 'matcha latte';
    });

    getProduct().then((value) {
      print('your product: $value');
    }).catchError((error) {
      print('sorry. $error');
    });
  }
}
