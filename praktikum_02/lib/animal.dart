abstract class Animal {
  var nama;
  var berat;

  Animal(this.nama, this.berat);

  void bersuara() {
    print('Suara Hewan');
  }
}

class Anjing extends Animal {
  Anjing(super.nama, super.berat);

  void lari() {
    print('lari lari');
  }

  @override
  void bersuara() {
    print('Guk guk');
  }
}

class Burung extends Animal {
  Burung(super.nama, super.berat, this.sayap);
  var sayap;

  void terbang() {
    print('terbang tinggi');
  }

  @override
  void bersuara() {
    print('kwak kwak');
  }
}
